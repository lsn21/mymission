Необходимо:
Оценить в часах и, после согласования, выполнить следующие задачи:
1. Авторизация
2. Получение списка статей
3. Вывод на экран списка статей
4. Получение и вывод на экран списка тренировок
Дизайн произвольный - можно на стандартных дефолтных экранах.
адрес: https://app.mirofs.demo.code-pilots.ru/
Твой логин: test@code-pilots.ru
Твой пароль: eapFI7fE
1) Авторизация POST /login
    form-data:
        email:{{user_email}}
        password:{{user_password}}
        
    Ответ:
        {"result":null,"error":{"code":0,"message":"Неверно введен email или пароль"}}
        или
        {"result": null,"error": null} + Заголовок X-AUTH-TOKEN: {{token}}
        
2) Получение списка статей GET /api/articles/articles
    
    Ответ:
        {
            error: null
            result: {
                count: 20
                current_page: 1
                demo_day: false
                items: [
                    0: {
                        category: {id: 1, name: "{{...}}"}
                        content_text: "{{...}}"
                        created_at: 1623272400
                        id: 1
                        is_favorite: false
                        is_special: false
                        main_image: "{{image_url}}"
                        title: "{{...}}"
                        updated_at: 1630875600
                        view: 144
                    }
                ]
                total: 234
                total_pages: 12
            }
        }
        
3) Получение списка тренировок GET /api/courses/weeks/{{course_id}}
    Ответ:
        {
            error: null
            result: {
                course: {
                    course_name: "HOME INTENSIVE"
                    description: "{{HTML-код описания}}"
                    id: 83
                    place: "UNIVERSAL"
                }
                demo_day: false
                place: {
                    available_places: null
                    can_change: false
                    current_place: {
                        id: "UNIVERSAL"
                        title: "Универсальный"
                    }
                }
                target: "Интенсивы"
                weeks: [
                    0: {
                        days: [
                            {
                                enabled: true
                                id: 577
                                img: "{{image_url}}"
                                name: "Ягодицы, ноги"
                                sort: 1
                            }
                        ]
                        id: 205
                        name: "Неделя 3"
                        sort: 3
                    }
                ]
            }
        }

