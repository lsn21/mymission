//
//  MainViewController.swift
//  MyMission
//
//  Created by Siarhei Lukyanau on 14.09.2021.
//

import UIKit
import Alamofire
import NVActivityIndicatorView

class MainViewController: UIViewController {

    @IBOutlet var authorizationView: UIView!
    @IBOutlet var loginTextField: UITextField!
    @IBOutlet var passwordTextField: UITextField!

    @IBOutlet var dataRetrievalView: UIView!
    
    var activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50), type: .circleStrokeSpin, color: .blue, padding: 0)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginTextField.text = "test@code-pilots.ru"
        passwordTextField.text = "eapFI7fE"
        title = "Авторизация"
        activityIndicator.center = view.center
        view.addSubview(activityIndicator)
}
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        title = " "
    }
    
    func authorizationView(show: Bool) {
        authorizationView.isHidden = !show
        dataRetrievalView.isHidden = show
    }

    @IBAction func authorizationButtonTapped(_ sender: Any) {
        var params = Parameters()
        params["email"] = loginTextField.text
        params["password"] = passwordTextField.text
        activityIndicator.startAnimating()
        Api.shared.login(params: params) { [weak self] loginAnswer, error in
            guard let weakSelf = self else { return }
            weakSelf.activityIndicator.stopAnimating()
            if loginAnswer != nil {
                print(loginAnswer as Any)
                if let error = loginAnswer?.error {
                    print(error.message as Any)
                    let alert = UIAlertController(title: "Ошибка", message: error.message, preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                    }))
                    weakSelf.present(alert, animated: true, completion: nil)
                }
                else {
                    weakSelf.title = "Списки"
                    weakSelf.authorizationView(show: false)
                }
            }
            else if let error = error {
                print("Ошибка: \(error.localizedDescription)")
            }
        }
    }
    
    @IBAction func articlesButtonTapped(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "ListArticlesViewController") as! ListArticlesViewController
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func coursesButtonTapped(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "CoursesViewController") as! CoursesViewController
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
}

extension MainViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
    }
}
