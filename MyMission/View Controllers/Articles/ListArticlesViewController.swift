//
//  ListArticlesViewController.swift
//  MyMission
//
//  Created by Siarhei Lukyanau on 14.09.2021.
//

import UIKit
import NVActivityIndicatorView

class ListArticlesViewController: UIViewController {

    @IBOutlet var articlesTableView: UITableView!
    
    var delegate: MainViewController?

    var articles: [Article]?
    
    var activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50), type: .circleStrokeSpin, color: .blue, padding: 0)

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Список статей"
        articlesTableView.register(UINib(nibName: "ArticlesTableViewCell", bundle: nil),                                  forCellReuseIdentifier: "ArticlesTableViewCell")
        activityIndicator.center = view.center
        view.addSubview(activityIndicator)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        activityIndicator.startAnimating()
        Api.shared.articles() { [weak self] (articlesAnswer, error) in
            guard let weakSelf = self else { return }
            weakSelf.activityIndicator.stopAnimating()
            if articlesAnswer != nil {
                print(articlesAnswer as Any)
                weakSelf.articles = articlesAnswer?.result?.items
                weakSelf.articlesTableView.reloadData()
            }
            else if let error = error {
                print("Ошибка: \(error.localizedDescription)")
                weakSelf.delegate?.authorizationView(show: true)
                weakSelf.navigationController?.popViewController(animated: true)
            }
        }
    }
}

extension ListArticlesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return articles?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ArticlesTableViewCell", for: indexPath as IndexPath)
        guard let articleCell = cell as? ArticlesTableViewCell else { return cell }
        
        let article = articles?[indexPath.row]
        articleCell.titleLabel.text = article?.title
        articleCell.categoryLabel.text = article?.category?.name
        articleCell.contentTextLabel.text = article?.contentText

        return articleCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
