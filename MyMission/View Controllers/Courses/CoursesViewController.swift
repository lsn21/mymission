//
//  CoursesViewController.swift
//  MyMission
//
//  Created by Siarhei Lukyanau on 15.09.2021.
//

import UIKit
import WebKit
import NVActivityIndicatorView

class CoursesViewController: UIViewController {

    @IBOutlet var courseNameLabel: UILabel!
    @IBOutlet var placeTileLabel: UILabel!
    @IBOutlet var targetLabel: UILabel!
    @IBOutlet var descriptionWebView: WKWebView!
    @IBOutlet var weeksTableView: UITableView!
    
    var delegate: MainViewController?

    var weeks: [Week]?
    
    var activityIndicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50), type: .circleStrokeSpin, color: .blue, padding: 0)

    override func viewDidLoad() {
        super.viewDidLoad()

        title = "Список тренировок"
        weeksTableView.register(UINib(nibName: "CoursesTableViewCell", bundle: nil),                                  forCellReuseIdentifier: "CoursesTableViewCell")
        activityIndicator.center = view.center
        view.addSubview(activityIndicator)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        activityIndicator.startAnimating()
        Api.shared.courses(id: 50516) { [weak self] (coursesAnswer, error) in
            guard let weakSelf = self else { return }
            weakSelf.activityIndicator.stopAnimating()
            if coursesAnswer != nil {
                print(coursesAnswer as Any)
                let result = coursesAnswer?.result
                
                weakSelf.courseNameLabel.text = result?.course?.courseName
                weakSelf.placeTileLabel.text = result?.place?.currentPlace?.title
                weakSelf.targetLabel.text = result?.target
                weakSelf.descriptionWebView.loadHTMLString(result?.course?.descript ?? "", baseURL: nil)
                var weeks = result?.weeks
                weeks?.sort { ($0.sort ?? 0) < ($1.sort ?? 0) }
                for i in 0..<(weeks?.count ?? 0) {
                    weeks?[i].days?.sort { ($0.sort ?? 0) < ($1.sort ?? 0) }
                }
                weakSelf.weeks = weeks
                weakSelf.weeksTableView.reloadData()
            }
            else if let error = error {
                print("Ошибка: \(error.localizedDescription)")
                weakSelf.delegate?.authorizationView(show: true)
                weakSelf.navigationController?.popViewController(animated: true)
            }
        }
    }
}

extension CoursesViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableView.automaticDimension
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        
        return weeks?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return weeks?[section].days?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return weeks?[section].name
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CoursesTableViewCell", for: indexPath as IndexPath)
        guard let courseCell = cell as? CoursesTableViewCell else { return cell }
        
        let day = weeks?[indexPath.section].days?[indexPath.row]
        courseCell.nameLabel.text = day?.name

        return courseCell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
