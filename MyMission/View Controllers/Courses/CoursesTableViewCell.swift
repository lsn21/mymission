//
//  CoursesTableViewCell.swift
//  MyMission
//
//  Created by Siarhei Lukyanau on 15.09.2021.
//

import UIKit

class CoursesTableViewCell: UITableViewCell {

    @IBOutlet var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
