//
//  ArticlesAnswer.swift
//  MyMission
//
//  Created by Siarhei Lukyanau on 14.09.2021.
//

import Foundation

// MARK: - ArticlesAnswer
struct ArticlesAnswer: Codable {
    
    let error: MMError?
    let result: ArticlesResult?

    enum CodingKeys: String, CodingKey {
        case error = "error"
        case result = "result"
    }
}

// MARK: - ArticlesResult
struct ArticlesResult: Codable {
    let count: Int?
    let currentPage: Int?
    let demoDay: Bool?
    let items: [Article]?
    let total: Int?
    let totalPages: Int?

    enum CodingKeys: String, CodingKey {
        case count = "count"
        case currentPage = "current_page"
        case demoDay = "demo_day"
        case items = "items"
        case total = "total"
        case totalPages = "total_pages"
    }
}

// MARK: - Articles
struct Article: Codable {
    
    let id: Int?
    let title: String?
    let contentText: String?
    let view: Int?
    let category: Category?
    let mainImage: String?
    let createdAt: Int?
    let updatedAt: Int?
    let isSpecial: Bool?
    let isFavorite: Bool?


    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
        case contentText = "content_text"
        case view = "view"
        case category = "category"
        case mainImage = "main_image"
        case createdAt = "created_at"
        case updatedAt = "updated_at"
        case isSpecial = "is_special"
        case isFavorite = "isFavorite"
    }
}

// MARK: - Category
struct Category: Codable {
    
    let id: Int?
    let name: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
    }
}
