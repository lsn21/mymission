//
//  LoginAnswer.swift
//  MyMission
//
//  Created by Siarhei Lukyanau on 14.09.2021.
//

import Foundation

// MARK: - LoginAnswer
struct LoginAnswer: Codable {
    
    let result: Result?
    let error: MMError?

    enum CodingKeys: String, CodingKey {
        case result = "result"
        case error = "error"
    }
}

// MARK: - Result
struct Result: Codable {
    
}

// MARK: - MMError
struct MMError: Codable {
    
    let code: Int?
    let message: String?

    enum CodingKeys: String, CodingKey {
        case code = "code"
        case message = "message"
    }
}



