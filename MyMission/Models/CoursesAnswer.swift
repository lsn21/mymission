//
//  CoursesAnswer.swift
//  MyMission
//
//  Created by Siarhei Lukyanau on 15.09.2021.
//

import Foundation

// MARK: - CoursesAnswer
struct CoursesAnswer: Codable {
    
    let error: MMError?
    let result: CoursesResult?

    enum CodingKeys: String, CodingKey {
        case error = "error"
        case result = "result"
    }
}

// MARK: - CoursesResult
struct CoursesResult: Codable {
    let weeks: [Week]?
    let course: Course?
    let place: Place?
    let target: String?
    let demo_day: Bool?

    enum CodingKeys: String, CodingKey {
        case weeks = "weeks"
        case course = "course"
        case place = "place"
        case target = "target"
        case demo_day = "demo_day"
    }
}

// MARK: - Week
struct Week: Codable {
    
    let id: Int?
    let name: String?
    let sort: Int?
    var days: [Day]?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case sort = "sort"
        case days = "days"
    }
}

// MARK: - Day
struct Day: Codable {
    
    let id: Int?
    let name: String?
    let sort: Int?
    let img: String?
    let enabled: Bool?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case name = "name"
        case sort = "sort"
        case img = "img"
        case enabled = "enabled"
    }
}

// MARK: - Course
struct Course: Codable {
    
    let id: Int?
    let courseName: String?
    let place: String?
    let descript: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case courseName = "course_name"
        case place = "place"
        case descript = "description"
    }
}

// MARK: - Place
struct Place: Codable {
    
    let canChange: Bool?
    let currentPlace: CurrentPlace?
    let availablePlaces: Bool?

    enum CodingKeys: String, CodingKey {
        case canChange = "can_change"
        case currentPlace = "current_place"
        case availablePlaces = "available_places"
    }
}

// MARK: - CurrentPlace
struct CurrentPlace: Codable {
    
    let id: String?
    let title: String?

    enum CodingKeys: String, CodingKey {
        case id = "id"
        case title = "title"
    }
}
