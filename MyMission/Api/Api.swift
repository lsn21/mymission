//
//  Api.swift
//  MyMission
//
//  Created by Siarhei Lukyanau on 14.09.2021.
//

import Foundation
import Alamofire

class Api {
    
    static let shared = Api()

    private var isInternet: Bool { Reachability.isInternetAvailable }
    
    // MARK: - login
    
    func login(params: Parameters, completion: @escaping (_ data: LoginAnswer?, _ error: Error?) -> Void) {
        customRequest(to: .login(params: params)) {(data, error) in completion(data, error) }
    }

    // MARK: - articles
    
    func articles(completion: @escaping (_ data: ArticlesAnswer?, _ error: Error?) -> Void) {
        customRequest(to: .articles) {(data, error) in completion(data, error) }
    }
    
    // MARK: - courses
    
    func courses(id: Int, completion: @escaping (_ data: CoursesAnswer?, _ error: Error?) -> Void) {
        customRequest(to: .courses(id: id)) {(data, error) in completion(data, error) }
    }
    
    // MARK: - customRequest
    
    private func customRequest<T: Decodable>(to route: Router, completion:  @escaping (_ data: T?, _ error: Error?) -> Void) {
        
        let path = Router.serverURL + route.path
        let parameters = route.parameters
        let method = route.method

        print("path: \(path)")
        print("method: \(method)")
        print("parameters: \(String(describing: parameters))")
        
        let urlString = Router.serverURL + route.path
        if let encoded = urlString.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed), let url = URL(string: encoded) {
            if isInternet {
                AF.request(url, method: route.method, parameters: route.parameters, encoding: JSONEncoding.default, headers: route.headers)
//                    .validate()
                    .responseDecodable { (response: DataResponse<T, AFError>) in
                        
                    switch response.result{
                    case .success:
                        if let result = response.value {
                            print(result)
                            if let headers = response.response?.headers.dictionary {
                                if let token = headers["x-auth-token"] {
                                    print(token as Any)
                                    UserDefaults.standard.set(token, forKey: "token")
                                }
                            }
                            completion(result, nil)
                        }
                        else {
                            completion(nil, nil)
                        }
                    case .failure(let error):
                        print(error.responseCode as Any)
                        print(error.errorDescription as Any)

                        completion(nil, error)
                    }
                }
            }
            else {
                completion(nil, nil)
            }
        }
    }
}
