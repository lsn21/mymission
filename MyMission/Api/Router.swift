//
//  Router.swift
//  MyMission
//
//  Created by Siarhei Lukyanau on 14.09.2021.
//

import Foundation
import Alamofire

enum Router {
    
    public static let serverURL = "https://app.mirofs.demo.code-pilots.ru"
    
    func getApiToken() -> String {
        let token = UserDefaults.standard.string(forKey: "token") ?? ""
        return token
    }

    case login(params: Parameters)
    case articles
    case courses(id: Int)
    
    var method: HTTPMethod {
        switch self {
            case    .login:
                return  .post
                
            case    .articles,
                    .courses:
                return  .get
        }
    }
    
    var path: String {
        switch self {
            case .login:            return "/login"
                
            case .articles:         return "/api/articles/articles"
            case .courses(let id):  return "/api/courses/weeks/\(id)"
        }
    }

    var parameters: Parameters? {
        switch self {
            case .login(let params):    return params

            default:  return nil
        }
    }
    
    var headers: HTTPHeaders {
        switch self {
            case .login:
                return [:]
            
            default:
                return ["x-auth-token" : getApiToken()]
        }
    }
    
}

